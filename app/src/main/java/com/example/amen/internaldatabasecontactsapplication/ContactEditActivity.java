package com.example.amen.internaldatabasecontactsapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.amen.internaldatabasecontactsapplication.controler.ContactsDatabaseHelper;
import com.example.amen.internaldatabasecontactsapplication.model.Contact;

public class ContactEditActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText editName, editSurname, editPhone;
    private Button saveButton;

    private int editedID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_edit);

        editName = (EditText) findViewById(R.id.editName);
        editSurname = (EditText) findViewById(R.id.editSurname);
        editPhone = (EditText) findViewById(R.id.editPhone);

        saveButton = (Button) findViewById(R.id.buttonSave);
        saveButton.setOnClickListener(this);

        if (getIntent().hasExtra("contact")) {
            Contact contact = (Contact) getIntent().getParcelableExtra("contact");
            editName.setText(contact.getFirstName());
            editSurname.setText(contact.getSurname());
            editPhone.setText(contact.getPhoneNumber());

            editedID = contact.getId();
        }
    }

    @Override
    public void onClick(View v) {
        Contact newContact = new Contact();

        newContact.setFirstName(editName.getText().toString());
        newContact.setSurname(editSurname.getText().toString());
        newContact.setPhoneNumber(editPhone.getText().toString());
        if (getIntent().hasExtra("contact")) {
            newContact.setId(editedID);
            new ContactsDatabaseHelper(getApplicationContext()).updateContact(newContact);
        } else {
            new ContactsDatabaseHelper(getApplicationContext()).createContact(newContact);
        }
        finish();
    }
}
