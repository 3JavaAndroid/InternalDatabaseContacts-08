package com.example.amen.internaldatabasecontactsapplication.controler;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.example.amen.internaldatabasecontactsapplication.model.Contact;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by amen on 1/21/17.
 */

public class ContactsDatabaseHelper extends SQLiteOpenHelper {

    private static final int DB_VERSION = 1;

    //
    private static final String TABLE_NAME = "contacts";
    private static final String DATABASE_NAME = "contacts.db";

    //
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_FIRST_NAME = "first_name";
    private static final String COLUMN_SURNAME = "last_surname";
    private static final String COLUMN_PHONE = "phone_number";

    public ContactsDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_NAME + " ( " +
                COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT , " +
                COLUMN_FIRST_NAME + " VARCHAR, " +
                COLUMN_SURNAME + " VARCHAR, " +
                COLUMN_PHONE + " VARCHAR); ");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public void createContact(Contact contact) {              // create from CRUD
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();

//        cv.put(COLUMN_ID, contact.getId());
        cv.put(COLUMN_FIRST_NAME, contact.getFirstName());
        cv.put(COLUMN_SURNAME, contact.getSurname());
        cv.put(COLUMN_PHONE, contact.getPhoneNumber());

        db.insert(TABLE_NAME, null, cv);
//        db.execSQL("INSERT INTO " + TABLE_NAME +
//                " ( " +
//                COLUMN_ID + ", " +
//                COLUMN_FIRST_NAME + ", " +
//                COLUMN_SURNAME + ", " +
//                COLUMN_PHONE + " )  VALUES "
//                + getValuesString(contact));
        db.close();
    }

    private String getValuesString(Contact contact) {
        return "( NULL ," +
                "'" + contact.getFirstName() + "', " +
                "'" + contact.getSurname() + "', " +
                "'" + contact.getPhoneNumber() + "')";
    }

    public List<Contact> readContacts() {                // read
        List<Contact> list = new ArrayList<>();

        SQLiteDatabase db = getReadableDatabase();
//        Cursor coursor = db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
        // select * from contacts where id = ?
        Cursor cursor = db.query(TABLE_NAME,
                new String[]{COLUMN_ID, COLUMN_FIRST_NAME, COLUMN_SURNAME, COLUMN_PHONE},
                null,   // fraza "where" wyszukiwania
                null,   // argumenty tej frazy where
                null,   // kolumna po której grupujemy
                null,   // fraza having
                null);  // kolejność/order

        for (int i = 0; i < cursor.getCount(); i++) {
            cursor.moveToNext();

            Contact newContact = new Contact();

            newContact.setId(cursor.getInt(0));
            newContact.setFirstName(cursor.getString(1));
            newContact.setSurname(cursor.getString(2));
            newContact.setPhoneNumber(cursor.getString(3));

            list.add(newContact);
        }

        return list;
    }

    public void updateContact(Contact contact) {              // update
        SQLiteDatabase db = getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put(COLUMN_FIRST_NAME, contact.getFirstName());
        cv.put(COLUMN_SURNAME, contact.getSurname());
        cv.put(COLUMN_PHONE, contact.getPhoneNumber());

        db.update(TABLE_NAME,
                cv,
                "id=?",
                new String[]{String.valueOf(contact.getId())});
//        db.execSQL(" UPDATE " + TABLE_NAME + " SET " +
//                COLUMN_FIRST_NAME + "='" + c.getFirstName() + "', " +
//                COLUMN_SURNAME + "='" + c.getSurname() + "', " +
//                COLUMN_PHONE + "='" + c.getPhoneNumber() + "' WHERE " +
//                COLUMN_ID + "='" + c.getId() + "'");

        db.close();
    }

    public void deleteContact(Contact c) {              // delete
        SQLiteDatabase db = getWritableDatabase();

        db.execSQL(" DELETE FROM " + TABLE_NAME + " WHERE " + COLUMN_ID + "='" + c.getId() + "'");

        db.close();
    }

}
